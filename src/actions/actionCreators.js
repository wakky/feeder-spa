import * as types from "./actionTypes";

export function addIngredient({name, desc}) {
  return {
    type: types.ADD_INGREDIENT,
    name,
    desc,
  };
}

export function removeIngredient({id}) {
  return {
    type: types.REMOVE_INGREDIENT,
    id,
  };
}

export function editIngredient({id, name, desc}){
  return {
    type: types.EDIT_INGREDIENT,
    id,
    name,
    desc,
  };
}