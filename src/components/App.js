import React from "react";
import {Switch, Route} from "react-router-dom";
import Ingredients from "./Ingredients";
import Home from "./Home";
import Meals from "./Meals";
import Plans from "./Plans";
import Users from "./Users";
import Login from "./Login";

class App extends React.Component {
  render() {
    return (
      <div>
        <header>Hello, we are testing now!</header>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/meals" component={Meals} />
          <Route path="/plans" component={Plans} />
          <Route path="/ingredients" component={Ingredients} />
          <Route path="/community" component={Users} />
          <Route path="/signup" component={Login} />
        </Switch>

      </div>
    );
  }
}

export default App;