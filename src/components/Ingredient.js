import React from "react";

class Ingredient extends React.Component {

  render() {
    const props = this.props;
    return (
      <div>
        <h6>{props.name}</h6>
        <p>{props.desc}</p>
        Nutrition list...
        Used in meals...
        Change quantity...
      </div>
    );
  }

}

export default Ingredient;