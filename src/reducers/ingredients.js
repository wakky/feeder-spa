import { ADD_INGREDIENT, REMOVE_INGREDIENT, EDIT_INGREDIENT } from "../actions/actionTypes";

function ingredients(state = [], action){
  const {type, ...payload} = action;
  switch (type) {
  case ADD_INGREDIENT:
    return [{...payload}, ...state];
  case REMOVE_INGREDIENT:
    return state.filter(elem => elem.id!==payload.id);
  case EDIT_INGREDIENT:
    return state.map(elem => elem.id===payload.id ? {...payload} : elem);
  default:
    return state;
  }
}

export default ingredients;