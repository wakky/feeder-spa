import * as actions from "../src/actions/actionCreators";
import * as types from "../src/actions/actionTypes";

test("it should create add ingredient action", () => {
  const params = {
    name: "Name",
    desc: "Desc",
  };
  const expected = {
    type: types.ADD_INGREDIENT,
    ...params,
  };
  expect(actions.addIngredient(params)).toEqual(expected);
});

test("it should create edit ingredient action", () => {
  const params = {
    id: 1,
    name: "Name",
    desc: "Desc",
  };
  const expected = {
    type: types.EDIT_INGREDIENT,
    ...params,
  };
  expect(actions.editIngredient(params)).toEqual(expected);
});

test("it should create remove ingredient action", () => {
  const params = {
    id: 1,
  };
  const expected = {
    type: types.REMOVE_INGREDIENT,
    ...params,
  };
  expect(actions.removeIngredient(params)).toEqual(expected);
});