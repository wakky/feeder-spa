import React from "react";
import {shallow} from "enzyme";
import Ingredient from "../../src/components/Ingredient";

test("ingredient should render name in h6 child", () => {
  const test = "Sample ingredient";
  const ingr = shallow(<Ingredient name={test} />);
  expect(ingr.contains(<h6>Sample ingredient</h6>)).toBeTruthy();
});

test("ingredient should render description in p child", () => {
  const test = "Sample desc";
  const ingr = shallow(<Ingredient desc={test} />);
  expect(ingr.contains(<p>Sample desc</p>)).toBeTruthy();
});