import ingredients from "../../src/reducers/ingredients";
import * as actions from "../../src/actions/actionCreators";

test("should return the initial state", () => {
  expect(ingredients(undefined, {})).toEqual([]);
});

test("should return current state in default", () => {
  expect(ingredients([], {})).toEqual([]);
});

test("should handle ADD_INGREDIENT", () => {
  const params = {
    name: "jhsjdhjdshd",
    desc: "jdjssdjsjjd",
  };
  expect(ingredients([], actions.addIngredient(params)))
    .toEqual([{...params}]);
});

test("should handle REMOVE_INGREDIENT", () => {
  const params = {
    id: "jd7js2jds",
  };
  expect(ingredients([{id: "jd7js2jds", name: "jhsjdhjdshd", desc: "jdjssdjsjjd",}], actions.removeIngredient(params)))
    .toEqual([]);
});

test("should return current state if there is no ingredient with id passed in REMOVE_INGREDIENT", () => {
  const params = {
    id: "884438883",
  };
  expect(ingredients([{id: "jd7js2jds", name: "jhsjdhjdshd", desc: "jdjssdjsjjd",}], actions.removeIngredient(params)))
    .toEqual([{id: "jd7js2jds", name: "jhsjdhjdshd", desc: "jdjssdjsjjd",}]);
});

test("should handle EDIT_INGREDIENT", () => {
  const params = {
    id: "jd7js2jds",
    name: "Some name",
    desc: "Some desc",
  };
  expect(ingredients([{id: "jd7js2jds", name: "jhsjdhjdshd", desc: "jdjssdjsjjd",}], actions.editIngredient(params)))
    .toEqual([{...params}]);
});

test("should return current state if there is no ingredient with id passed in EDIT_INGREDIENT", () => {
  const params = {
    id: "747748",
    name: "Some name",
    desc: "Some desc",
  };
  expect(ingredients([{id: "jd7js2jds", name: "jhsjdhjdshd", desc: "jdjssdjsjjd",}], actions.editIngredient(params)))
    .toEqual([{id: "jd7js2jds", name: "jhsjdhjdshd", desc: "jdjssdjsjjd",}]);
});